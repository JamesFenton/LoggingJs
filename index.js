var express = require('express');
var bodyParser = require('body-parser');
var pgp = require("pg-promise")(/*options*/);

var cn = {
    host: 'localhost', // server name or IP address;
    port: 5432,
    database: 'logs',
    user: 'postgres',
    password: process.env.logging_password
};
var db = pgp(cn);

var app = express();

app.use(bodyParser.json());

app.post('/log', function (req, res) {
  var log = req.body;
  console.log("Got log request:");
  console.log(log.toString());
  db.one("INSERT INTO logs (time, app, machine, level, message, status_code, response_time)\
                  VALUES ($1,   '$2', '$3',   '$4',  '$5',    $6,          $7);",
                  [new Date(log.time), log.app, log.machine, log.level, log.message, log.status_code, log.response_time])
  .then(function (data) {
      console.log("DATA:", data.value);
      res.status(200).send("Inserted log");
  })
  .catch(function (error) {
      console.log("ERROR:", error);
      res.status(500).send("Error: " + error);
  });
});

app.get('/test', function(req, res){
  res.status(200).send();
});

app.listen(3000, function () {
  console.log('Logging listening on port 3000!');
});
